var firstClickOnProductLinksTab = true;
jQuery(document).ready(function($) {
	if ( typeof($.fn.DataTable) != 'undefined' && $('.productLinks_options').length)
	{
		$('.productLinks_options').on('click', function(){
			if (firstClickOnProductLinksTab)
			{
				firstClickOnProductLinksTab = false;
				setTimeout(function(){
					$('#productLinksTable').DataTable( {
						order: [[ 1, "desc" ]],
						colReorder: true,
						scrollX: true,
						pagingType: 'full',
						language: {
									paginate: {
										first:    '«',
										previous: '‹',
										next:     '›',
										last:     '»'
									}
								},
					} );
				}, 500);
			}
		});
			
	}
});